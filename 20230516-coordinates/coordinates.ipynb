{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "115a5243-08f4-4949-83d1-67b3db012014",
   "metadata": {},
   "source": [
    "# Coordinates transforms and remapping with SunPy\n",
    "\n",
    "Based on the [SunPy examples gallery](https://docs.sunpy.org/en/stable/generated/gallery/index.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a1164981-8276-4f92-9126-6e10b3c30097",
   "metadata": {},
   "source": [
    "## Getting the data we will use\n",
    "\n",
    "We get some AIA, EUI, and SPICE files for 2022-04-02.\n",
    "\n",
    "* EUI and SPICE directly by URL, for simplicity (otherwise one could use `sunpy.net.Fido` to find them through VSO).\n",
    "* AIA from MEDOC using PySitools2 (we could also have used VSO using `sunpy.net.Fido`)\n",
    "\n",
    "We use `parfive` for parallel download."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d9267691-7a09-41b5-9917-962e494081f3",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "from datetime import datetime\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "import astropy.units as u\n",
    "from astropy.coordinates import SkyCoord\n",
    "from astropy.wcs import WCS\n",
    "from reproject import reproject_interp\n",
    "from reproject.mosaicking import reproject_and_coadd\n",
    "import parfive\n",
    "\n",
    "from sunpy.map import Map, make_heliographic_header, make_fitswcs_header, all_coordinates_from_map\n",
    "from sunpy.coordinates import Helioprojective, get_body_heliographic_stonyhurst\n",
    "from sunraster.instr.spice import read_spice_l2_fits\n",
    "\n",
    "from sitools2 import SdoClientMedoc\n",
    "\n",
    "%matplotlib widget\n",
    "plt.rcParams[\"figure.figsize\"] = (10,8) # larger default figure size"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41da4cda-23a9-481d-9cfc-240b6cd97226",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Search for AIA 171 map at MEDOC\n",
    "sdo_client = SdoClientMedoc()\n",
    "aia_data_list = sdo_client.search(dates=[datetime(2022, 4, 2, 10), datetime(2022, 4, 2, 10, 2)], waves=[171])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c1a2de0-f9a9-4486-a96c-92b1fd07ca8a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# dict of URLs, one file per instrument, for this tutorial\n",
    "urls = {\n",
    "    'aia': aia_data_list[0].url,\n",
    "    'fsi': 'https://www.sidc.be/EUI/data/releases/202301_release_6.0/L2/2022/04/02/solo_L2_eui-fsi174-image_20220402T100045611_V01.fits',\n",
    "    'hri': 'https://www.sidc.be/EUI/data/releases/202301_release_6.0/L2/2022/04/02/solo_L2_eui-hrieuv174-image_20220402T100005600_V01.fits',\n",
    "    'spice': 'https://spice.osups.universite-paris-saclay.fr/spice-data/release-3.0/level2/2022/04/02/solo_L2_spice-n-ras_20220402T101536_V06_100664001-000.fits',\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26e308fa-c695-41c7-b160-a7052960e2df",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Download data to files/ directory (if they are not already there)\n",
    "download_dir = Path('files')\n",
    "download_dir.mkdir(exist_ok=True)\n",
    "downloader = parfive.Downloader()\n",
    "for instrument in urls:\n",
    "    downloader.enqueue_file(urls[instrument], download_dir, f'{instrument}.fits')\n",
    "downloader.download()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "58926133-2599-4250-b80a-fd70a4264663",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build dictionary of maps for all image files (not SPICE)\n",
    "# Resampling is optional, this is to speed up the tutorial running time\n",
    "maps = {\n",
    "    instrument: Map(\n",
    "        download_dir / f'{instrument}.fits'\n",
    "    ).resample((1024, 1024)*u.pix)\n",
    "    for instrument in urls if instrument != 'spice'\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91848b2d-1b8f-4040-8e42-3387770f8ea2",
   "metadata": {},
   "source": [
    "## Compare EUI and AIA\n",
    "\n",
    "The viewpoints [are different](https://solar-mach.streamlitapp.com/?embedded=true&date=20220402&time=1000&coord_sys=0&long_offset=270&bodies=STEREO+A&bodies=Earth&bodies=BepiColombo&bodies=Parker+Solar+Probe&bodies=Solar+Orbiter&speeds=400&speeds=400&speeds=400&speeds=400&speeds=400&). Earth to Solar Orbiter longitude difference is 108.5°."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7e438d18-84c7-48ae-a522-5eadbe1a6959",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "ax = fig.add_subplot(projection='polar')\n",
    "sun = plt.Circle(\n",
    "    (0.0, 0.0), (10*u.Rsun).to_value(u.AU),\n",
    "    transform=ax.transProjectionAffine + ax.transAxes, color=\"yellow\",\n",
    "    alpha=1, label=\"Sun\"\n",
    ")\n",
    "ax.add_artist(sun)\n",
    "\n",
    "for instrument in ['aia', 'fsi']:\n",
    "    coordinates = maps[instrument].observer_coordinate\n",
    "    ax.plot(coordinates.lon.to('rad'), coordinates.radius.to(u.AU), 'o', label=maps[instrument].observatory)\n",
    "ax.set_theta_zero_location(\"S\")\n",
    "ax.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c85e02bc-40cd-45cc-b702-d94e039f55c8",
   "metadata": {},
   "outputs": [],
   "source": [
    "maps['aia']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e27d0b83-9ca1-4afc-b12d-72572a01aabd",
   "metadata": {},
   "outputs": [],
   "source": [
    "maps['fsi']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "903656cc-23f5-4259-a9c0-75d771f97ca5",
   "metadata": {},
   "source": [
    "## Select a point and region on AIA map, plot them on FSI map\n",
    "\n",
    "Helioprojective coordinates are defined for a given observer (including position and time), the corresponding frame can be obtained from the map.\n",
    "\n",
    "We first need to ensure that the solar radius is the same in each map. We then have to adjust the radius in the AIA map so that it is consistent with the other ones.\n",
    "\n",
    "This radius actually is the IAU official solar radius constant. Reprojections of the corona should use a different radius for the sphere on which images are projected, but this is out of the scope of this tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2de707ea-815c-46c2-90f2-3ff22675d258",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print solar reference radii\n",
    "for i in maps:\n",
    "    print(i, maps[i].meta['rsun_ref'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a238d113-9464-43e3-87dc-382232d201b7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# These are not consistent, we then adjust the radius for AIA to have the same value as the others\n",
    "original_aia_rsun = maps['aia'].meta['rsun_ref']  # keep for later\n",
    "maps['aia'].meta['rsun_ref'] = maps['fsi'].meta['rsun_ref']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "108a265a-4db7-49c2-9c8b-34688719769e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# This is the IAU radius:\n",
    "from astropy.constants import R_sun\n",
    "print(R_sun)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26ff93ad-e33f-4c20-aae9-6f0e58545610",
   "metadata": {},
   "outputs": [],
   "source": [
    "# A point on the Sun defined by its helioprojective coordinates as seen from AIA\n",
    "aia_feature = SkyCoord(800 * u.arcsec, 300 * u.arcsec, frame=maps['aia'].coordinate_frame)\n",
    "aia_feature"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "190e8b36-4ee2-45bc-bcda-c9103c338889",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Corners of a rectangle in the same coordinates frame\n",
    "aia_region = {\n",
    "    'bottom_left': SkyCoord(750 * u.arcsec, 200 * u.arcsec, frame=maps['aia'].coordinate_frame),\n",
    "    'top_right': SkyCoord(850 * u.arcsec, 400 * u.arcsec, frame=maps['aia'].coordinate_frame),\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "59ed88ec-0a48-4bbb-9835-54c3f7f1232a",
   "metadata": {},
   "source": [
    "Plot point and region on AIA map."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "02b53bb4-e487-4c99-9be8-f1bd83abc03b",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "ax = fig.add_subplot(projection=maps['aia'])\n",
    "maps['aia'].plot(axes=ax)\n",
    "ax.plot_coord(aia_feature, 'bo')\n",
    "maps['aia'].draw_quadrangle(**aia_region, edgecolor='r')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2e7c504-f7e4-4801-88f9-dcf2ac393a77",
   "metadata": {},
   "source": [
    "Plot same point and region on FSI map. Coordinate transforms are handled automagically."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "47cf5682-e533-4e56-9eab-350db19fb62b",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "ax = fig.add_subplot(projection=maps['fsi'])\n",
    "maps['fsi'].plot(axes=ax)\n",
    "ax.plot_coord(aia_feature, 'bo')\n",
    "maps['fsi'].draw_quadrangle(**aia_region, edgecolor='r')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ea378a4a-edb8-4a56-b0d3-93519fb33557",
   "metadata": {},
   "source": [
    "## Reproject AIA map to FSI point of view\n",
    "\n",
    "SunPy gallery:\n",
    "\n",
    "* [Reprojecting images to different observers](https://docs.sunpy.org/en/stable/generated/gallery/map_transformations/reprojection_different_observers.html)\n",
    "* [Reprojecting using a spherical screen](https://docs.sunpy.org/en/stable/generated/gallery/map_transformations/reprojection_spherical_screen.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dd46db13-3a25-45a0-bb54-042f72f2d5b5",
   "metadata": {},
   "outputs": [],
   "source": [
    "def do_plot(map1, map2, map1to2):\n",
    "    'Plot map1, and map2 reprojected to the map1 view'\n",
    "    fig = plt.figure(figsize=(10,5))\n",
    "    ax1 = fig.add_subplot(1, 2, 1, projection=map2)\n",
    "    map2.plot(axes=ax1, title='FSI map')\n",
    "    map2.draw_grid(color='w', system='carrington')\n",
    "\n",
    "    ax2 = fig.add_subplot(1, 2, 2, projection=map2)\n",
    "    map1to2.plot(axes=ax2, title='AIA observation as seen from Solar Orbiter')\n",
    "    map1.draw_grid(color='w', system='carrington')\n",
    "    map1.draw_limb(color='blue')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1c8b566-6e89-4e6c-934b-34fb02030bdd",
   "metadata": {},
   "source": [
    "Assuming a reprojection to the solar sphere (default):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "867d5690-76e8-442b-a938-5c7227e7e1f1",
   "metadata": {},
   "outputs": [],
   "source": [
    "aia_viewed_from_fsi = maps['aia'].reproject_to(maps['fsi'].wcs)\n",
    "# adding algorithm='adaptive' uses the DeForest (2004) algorithm:\n",
    "# https://reproject.readthedocs.io/en/stable/celestial.html#adaptive-resampling\n",
    "do_plot(maps['aia'], maps['fsi'], aia_viewed_from_fsi)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "59ace64f-d51c-4308-8853-be1585d923b3",
   "metadata": {},
   "source": [
    "Assuming reprojection to the celestial sphere:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "73d823cc-278c-42ce-a125-d8f98e63f725",
   "metadata": {},
   "outputs": [],
   "source": [
    "with Helioprojective.assume_spherical_screen(maps['aia'].observer_coordinate):\n",
    "    aia_viewed_from_fsi = maps['aia'].reproject_to(maps['fsi'].wcs)\n",
    "do_plot(maps['aia'], maps['fsi'], aia_viewed_from_fsi)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a2ea87b7-3f66-4914-9abb-49cccf60d5de",
   "metadata": {},
   "source": [
    "We don't see the celestial sphere projection because the angle with Earth is > 90° (unlike in [SunPy example](https://docs.sunpy.org/en/stable/generated/gallery/map_transformations/reprojection_spherical_screen.html)).\n",
    "\n",
    "Assuming reprojection to the celestial sphere off-disk, on the solar sphere on-disk:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d0b97574-5f04-4463-af79-7b5d527008f0",
   "metadata": {},
   "outputs": [],
   "source": [
    "with Helioprojective.assume_spherical_screen(maps['aia'].observer_coordinate, only_off_disk=True):\n",
    "    aia_viewed_from_fsi = maps['aia'].reproject_to(maps['fsi'].wcs)\n",
    "do_plot(maps['aia'], maps['fsi'], aia_viewed_from_fsi)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68d3588e-189b-4b84-b633-ea40403651de",
   "metadata": {},
   "source": [
    "Same issue here (off-disk), because of Earth-Sun-Solar Orbiter angle >90°. Better try FSI from Mercury view (69.1°)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30a4a8b9-86ee-41f0-b94e-fe5a89e38138",
   "metadata": {},
   "outputs": [],
   "source": [
    "mercury = get_body_heliographic_stonyhurst('mercury', maps['fsi'].date)\n",
    "mercury"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9cef6874-5045-4fb8-907c-b234cd3d1e2c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# center solar disk in Mercury-based helioprojective coordinates\n",
    "mercury_ref_coord = SkyCoord(\n",
    "    0*u.arcsec, 0*u.arcsec,\n",
    "    obstime=maps['fsi'].reference_coordinate.obstime,\n",
    "    observer=mercury,\n",
    "    rsun=maps['fsi'].reference_coordinate.rsun,\n",
    "    frame=\"helioprojective\"\n",
    ")\n",
    "header = make_fitswcs_header(\n",
    "    (512, 512),\n",
    "    mercury_ref_coord,\n",
    "    scale=[30, 30] * u.arcsec / u.pix,\n",
    ")\n",
    "with Helioprojective.assume_spherical_screen(maps['fsi'].observer_coordinate, only_off_disk=True):\n",
    "    fsi_viewed_from_mercury = maps['fsi'].reproject_to(header)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26ca6226-a5c6-404d-8086-3a4b03e2f4ed",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "fsi_viewed_from_mercury.plot()\n",
    "fsi_viewed_from_mercury.draw_grid()\n",
    "fsi_viewed_from_mercury.draw_limb()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f9718777-19a9-4fa8-ba46-fa8a466b7cf6",
   "metadata": {},
   "source": [
    "## Build Carrington map from AIA and FSI\n",
    "\n",
    "SunPy gallery:\n",
    "\n",
    "* [Creating Carrington maps](https://docs.sunpy.org/en/stable/generated/gallery/map_transformations/reprojection_carrington.html)\n",
    "* [Creating a full-Sun map with AIA and EUVI](https://docs.sunpy.org/en/stable/generated/gallery/map_transformations/reprojection_aia_euvi_mosaic.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3332a558-50f4-46c1-890f-3cb15e85892e",
   "metadata": {},
   "outputs": [],
   "source": [
    "shape_out = (720, 1440)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "be2ec2e5-af80-4810-a6d3-748567fd5adc",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check reference solar radius (again)\n",
    "for i in maps:\n",
    "    print(i, maps[i].meta['rsun_ref'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "debf4dcc-b121-4e99-9c18-55764142f831",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Back to the original AIA reference solar radius (or not; now confused about what should be done)\n",
    "maps['aia'].meta['rsun_ref'] = 695700000 # original_aia_rsun\n",
    "for i in maps:\n",
    "    print(i, maps[i].meta['rsun_ref'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37266f4d-358c-4af9-bf32-1205ce60d2a2",
   "metadata": {},
   "outputs": [],
   "source": [
    "for instrument in ['aia', 'fsi']:\n",
    "    header = make_heliographic_header(maps[instrument].date, maps[instrument].observer_coordinate, shape_out, frame='carrington')\n",
    "    carr_map = maps[instrument].reproject_to(WCS(header))\n",
    "    plt.figure(figsize=(10,6))\n",
    "    carr_map.plot()\n",
    "    maps[instrument].draw_limb(color='b')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "872562d9-57b0-4d8d-ad20-57cb771632ed",
   "metadata": {},
   "source": [
    "Now combine both projected images into one map."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a34c1c71-afa3-470e-8787-05b405dcf79f",
   "metadata": {},
   "outputs": [],
   "source": [
    "header = make_heliographic_header(maps['aia'].date, maps['aia'].observer_coordinate, shape_out, frame='carrington')\n",
    "array, footprint = reproject_and_coadd(\n",
    "    [maps[instrument] for instrument in ['aia', 'fsi']],\n",
    "    WCS(header),\n",
    "    shape_out,\n",
    "    reproject_function=reproject_interp,\n",
    "    match_background=True, background_reference=0\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9878d2c2-82d6-4083-9c5c-1610b7cc2ea1",
   "metadata": {},
   "outputs": [],
   "source": [
    "carr_map = Map((array, header))\n",
    "carr_map.plot_settings = maps['aia'].plot_settings\n",
    "fig = plt.figure(figsize=(15, 8))\n",
    "ax = fig.add_subplot(projection=WCS(header))\n",
    "im = carr_map.plot(axes=ax, vmin=100)\n",
    "plt.colorbar(im, ax=ax)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45dd8e25-9ea3-496f-a40c-d74566886ba8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Same with weights\n",
    "coordinates = tuple(map(all_coordinates_from_map, [maps[i] for i in ['aia', 'fsi']]))\n",
    "weights = [coord.transform_to(\"heliocentric\").z.value for coord in coordinates]\n",
    "weights = [(w / np.nanmax(w)) ** 3 for w in weights]\n",
    "for w in weights:\n",
    "    w[np.isnan(w)] = 0\n",
    "plt.figure()\n",
    "plt.imshow(weights[0])\n",
    "plt.colorbar()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c28fa09-4ab8-46b8-82de-d1e9a60435f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "header = make_heliographic_header(maps['aia'].date, maps['aia'].observer_coordinate, shape_out, frame='carrington')\n",
    "array, footprint = reproject_and_coadd(\n",
    "    [maps[instrument] for instrument in ['aia', 'fsi']],\n",
    "    WCS(header),\n",
    "    shape_out,\n",
    "    input_weights=weights,\n",
    "    reproject_function=reproject_interp,\n",
    "    match_background=True, background_reference=0\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d05b6c7e-e869-4fbd-95a2-dbf36ad1fa77",
   "metadata": {},
   "outputs": [],
   "source": [
    "carr_map = Map((array, header))\n",
    "carr_map.plot_settings = maps['aia'].plot_settings\n",
    "fig = plt.figure(figsize=(15, 8))\n",
    "ax = fig.add_subplot(projection=WCS(header))\n",
    "im = carr_map.plot(axes=ax, vmin=100)\n",
    "plt.colorbar(im, ax=ax)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e0e56ba-4312-4e45-9d36-803757585810",
   "metadata": {},
   "source": [
    "## Plot SPICE FOV on FSI"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bf34368b-1125-4ac5-905f-9b240c9a4a83",
   "metadata": {},
   "outputs": [],
   "source": [
    "raster = read_spice_l2_fits(str(download_dir / 'spice.fits'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7da36035-ecb2-44fa-bf29-b27fe7282450",
   "metadata": {},
   "outputs": [],
   "source": [
    "raster"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "53dc142d-decf-4d3c-b15b-4a1cb7ed0b11",
   "metadata": {},
   "outputs": [],
   "source": [
    "window = raster['Ne VIII 770 - Peak']\n",
    "window"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "72d776a2-3ced-4b25-ac97-a8eed113625d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use rebin to create intensity map from spectrogram (data cube)\n",
    "intensity = window.rebin((1,50,1,1))[0, 0, :, :]\n",
    "# bug: y axis becomes incorrect when binning over y?\n",
    "maps['spice'] = Map((intensity.data, intensity.meta))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d81c5b5d-8541-4889-8147-96c88b246dbf",
   "metadata": {},
   "outputs": [],
   "source": [
    "intensity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a9ff70ff-9d67-4aaf-8620-1b495ca7c555",
   "metadata": {},
   "outputs": [],
   "source": [
    "# For better image value normalization\n",
    "from astropy.visualization import SqrtStretch, AsymmetricPercentileInterval, ImageNormalize\n",
    "norm = ImageNormalize(\n",
    "    intensity.data,\n",
    "    interval=AsymmetricPercentileInterval(1, 99),\n",
    "    stretch=SqrtStretch()\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a775c0f2-b888-47c1-b8e2-247c296eb334",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "maps['spice'].plot(norm=norm, aspect=1/4)\n",
    "plt.colorbar()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5a45a343-6396-4530-8e03-75cbe668892d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Composite map (need to understand how to apply value normalization to SPICE image)\n",
    "comp_map = Map(maps['hri'], maps['spice'], composite=True)\n",
    "plt.figure()\n",
    "comp_map.plot()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0db4aaea-4dd1-4bde-a53d-7526379f2d33",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Composite map with HRI contours\n",
    "map_spice2hri = maps['spice'].reproject_to(maps['hri'].wcs)\n",
    "comp_map = Map(map_spice2hri, maps['hri'], composite=True)\n",
    "comp_map.set_levels(index=1, levels=[0, 1000, 2000, 5000]*u.ct/u.s)\n",
    "plt.figure()\n",
    "comp_map.plot(norm=norm)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3ed38e8c-fcf9-4062-a6cf-ccb9d5c882f0",
   "metadata": {},
   "outputs": [],
   "source": [
    "from mpl_animators import ArrayAnimatorWCS\n",
    "from astropy.visualization import AsinhStretch, ImageNormalize\n",
    "map_sequence = Map(maps['hri'], map_spice2hri, sequence=True)\n",
    "sequence_array = map_sequence.as_array()\n",
    "norm = ImageNormalize(vmin=0, vmax=3e4, stretch=AsinhStretch(0.01))\n",
    "\n",
    "sequence_array[..., 1] *= 4000 # rescale SPICE values to EUI values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3c79b80f-9eb2-4714-b98d-b8fa9cb5ee96",
   "metadata": {},
   "outputs": [],
   "source": [
    "m = map_sequence[0]\n",
    "wcs = WCS(naxis=3)\n",
    "wcs.wcs.crpix = u.Quantity([0*u.pix] + list(m.reference_pixel))\n",
    "wcs.wcs.cdelt = [1] + list(u.Quantity(m.scale).value)\n",
    "wcs.wcs.crval = [0, m._reference_longitude.value, m._reference_latitude.value]\n",
    "wcs.wcs.ctype = ['index'] + list(m.coordinate_system)\n",
    "wcs.wcs.cunit = [u.dimensionless_unscaled] + list(m.spatial_units)\n",
    "wcs.wcs.aux.rsun_ref = m.rsun_meters.to_value(u.m)\n",
    "print(wcs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eb4e870e-f7d5-4435-8365-66a0338ab88b",
   "metadata": {},
   "outputs": [],
   "source": [
    "wcs_anim = ArrayAnimatorWCS(sequence_array, wcs, [0, 'x', 'y'], norm=norm).get_animation()\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
