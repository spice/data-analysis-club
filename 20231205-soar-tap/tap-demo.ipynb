{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "12291595-e831-4c98-a31e-8a5687d6e75e",
   "metadata": {},
   "source": [
    "# Fun with the SOAR TAP API!\n",
    "\n",
    "## Introduction\n",
    "\n",
    "The SOAR ([Solar Orbiter Archive](https://soar.esac.esa.int/soar/)) has a web query form, but, as all query forms, the functionalities are quite limited, and the results web page cannot easily be used by a script.\n",
    "\n",
    "To access a wider range of functionalities and to be able to query the archive using scripts, SOAR has an API ([Application Programming Interface](https://en.wikipedia.org/wiki/API)). This API follows the IVOA ([International  Virtual  Observatory  Alliance](https://www.ivoa.net/)) TAP ([Table Access Protocol](https://www.ivoa.net/documents/TAP/20190927/)) protocol. The general idea is that database tables (or views) are exposed via this interface, so that the metadata they contain can be queried. Queries are written in a standard language and are encoded in URLs, interpreted by SOAR, and the result is returned as a file in a standard format.\n",
    "\n",
    "The TAP access to SOAR is documented in a [series of pages](https://www.cosmos.esa.int/web/soar/guide-to-using-tap) in the SOAR documentation: [table contents](https://www.cosmos.esa.int/web/soar/tables-views-and-columns), synchronous and asynchronous queries, how to use TAP to do queries and the metadata and to retrieve data files...\n",
    "\n",
    "Some of the functionalities can be used at a higher level through the [sunpy-soar](https://docs.sunpy.org/projects/soar/) plugin of SunPy's Fido. Here we are going to concentrate on the lower level.\n",
    "\n",
    "The SOAR TAP API is also used:\n",
    "\n",
    "* by [SODA](https://github.com/ebuchlin/soda/) to produce [data availability plots](https://spice.ias.u-psud.fr/spice-data/data-availability/index-old.html)\n",
    "* by the [SPICE data release tool](https://git.ias.u-psud.fr/spice/data_release_tool/) to check that files sent to SOAR are effectively available."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b19f247d-c02a-4ac1-8bb5-566ab9c987de",
   "metadata": {},
   "source": [
    "## TAP web API\n",
    "\n",
    "The TAP endpoint (the base URL) for SOAR is http://soar.esac.esa.int/soar-sl-tap/tap. This URL just provides a page with the list of commands.\n",
    "\n",
    "The URL for a TAP query then includes a command and its argument.\n",
    "\n",
    "For example, the tables command provides a list of tables: http://soar.esac.esa.int/soar-sl-tap/tap/tables, which returns a JSON file.\n",
    "\n",
    "In Python, this can be handled using the following packages:\n",
    "\n",
    "* [`requests`](https://docs.python-requests.org/en/latest/index.html) does HTTP requests and simply provides the result of the request. If it is in JSON format, it can easily be deserialized into a Python object.\n",
    "* [`astroquery.utils.tap`](https://astroquery.readthedocs.io/en/latest/utils/tap.html) provides a higher-level interface to TAP, it returns astropy Tables.\n",
    "\n",
    "We will start using `requests` so we can see what happens at the lower level."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2af61ae4-e9ea-4e6d-a6e1-8b96f7d4fde3",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import requests\n",
    "soar_end_point = 'http://soar.esac.esa.int/soar-sl-tap'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "75ada9f4-9dca-42af-9f65-59b9abdf6ad7",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "r = requests.get(soar_end_point + '/tap/tables')\n",
    "r"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4309279e-0929-4b40-a654-2dd7c3be93f8",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "r.content.decode()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f3a1577-509a-4089-aede-ee376cfa5004",
   "metadata": {},
   "source": [
    "The HTTP GET request was successful (code 200) and we got some content. This a an XML file encoded in a series of bytes (this was not a Python `str` before using `.decode()`). When using directly `requests`, we can prefer getting a JSON file. This is done by adding a `FORMAT` parameter to the HTTP GET request."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "58ae8179-fe09-40fb-b9c0-7557e77b01dc",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "r = requests.get(soar_end_point + '/tap/tables', params={'FORMAT': 'json'})\n",
    "r.content.decode()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eececaad-ab43-408e-9992-82627b9964e8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# The actual full URL for the GET request, including parameters\n",
    "r.url"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d804ed91-d5a0-4415-b064-1c780984e677",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# The JSON content de-serialized as a Python object\n",
    "result = r.json()\n",
    "result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ea40caf2-8d7f-4626-90ec-3efd12cbe6f6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# List of all table \"namespaces\" (not sure this is the correct word)\n",
    "[(i, d['name']) for i, d in enumerate(result)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f1b6bb60-ed44-4563-b348-ddefdbbf248b",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# List of all tables in soar \"namespace\"\n",
    "[(i, d['name']) for i, d in enumerate(result[2]['tables'])]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5a3dd041-8679-41cc-9dda-1db93944c1c2",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# List of all columns in soar.v_sc_data_item (all files, only latest version of each file)\n",
    "[(i, d['name']) for i, d in enumerate(result[2]['tables'][45]['columns'])]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df3d48e4-0a2e-4264-9bb4-390b175a8c03",
   "metadata": {},
   "source": [
    "These tables and columns are [documented in the SOAR help pages](https://www.cosmos.esa.int/web/soar/tables-views-and-columns)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29ee44e3-161b-461f-a3ce-70eaee502a05",
   "metadata": {},
   "source": [
    "## ADQL queries using `requests`\n",
    "\n",
    "ADQL is the Astronomical Data Query Language, it is an [IVOA standard](https://www.ivoa.net/documents/cover/ADQL-20081030.html) for a SQL-like language with additions for astronomy (coordinates, search by region, angles, intersections of regions...). This would be a simple query to display some columns of the `soar.v_spi_sc_fits` table with some conditions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "51a045a3-db15-4403-a822-b5ba09585f06",
   "metadata": {},
   "outputs": [],
   "source": [
    "adql_query = \"SELECT TOP 10 filename, filesize FROM soar.v_sc_data_item WHERE instrument='SPICE' ORDER BY insertion_time DESC\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9bd2c551-61ee-4f57-816d-47330b162f04",
   "metadata": {},
   "source": [
    "This will display the file names of the last 10 SPICE files ingested into SOAR.\n",
    "\n",
    "Synchronous ADQL queries are done with the `sync` TAP \"resource\" using the query as parameter. A simple function to do this would be:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "32ac09c4-bbf5-4a2e-b047-e96f00830f2d",
   "metadata": {},
   "outputs": [],
   "source": [
    "def soar_sync_query(adql_query):\n",
    "    payload = {\n",
    "        'REQUEST': 'doQuery',\n",
    "        'LANG': 'ADQL',\n",
    "        'FORMAT': 'json',\n",
    "        'QUERY': adql_query\n",
    "    }\n",
    "    r = requests.get(f'{soar_end_point}/tap/sync', params=payload)\n",
    "    print(f'HTTP GET URL for the query: {r.url}')\n",
    "    r.raise_for_status()\n",
    "    return r.json()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6d388df4-19c0-488f-a918-92952a2025a6",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = soar_sync_query(adql_query)\n",
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d68d8e6e-c7a2-4ab9-bdd1-b75094705472",
   "metadata": {},
   "source": [
    "In the result, there are first metadata about the table columns, then the table data. One can then get the file list simply by"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "76375193-485e-4fde-a95d-9bd0453a51a0",
   "metadata": {},
   "outputs": [],
   "source": [
    "[r[0] for r in result['data']]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2290a966-91ce-4555-996e-97165066e6e4",
   "metadata": {},
   "source": [
    "## Queries using astroquery TAP\n",
    "\n",
    "The same can be achieved using `astroquery.utils.tap`, then we get astropy Tables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "756d06c8-a786-4780-925a-ace5ed0cff70",
   "metadata": {},
   "outputs": [],
   "source": [
    "from astroquery.utils.tap.core import TapPlus\n",
    "soar = TapPlus(url=f\"{soar_end_point}/tap\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "409439f4-14dd-4b0f-8a88-c7eeb0d2909b",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Display list of tables\n",
    "tables = soar.load_tables()\n",
    "for table in tables:\n",
    "    print(table.get_qualified_name())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a788ece9-0bba-4802-b82b-0373719cb1cc",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Display list of columns for a table\n",
    "table = soar.load_table('soar.v_sc_data_item')\n",
    "for column in table.columns:\n",
    "    print(column.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "792bca23-0c8d-4109-be3f-8869cd5d713f",
   "metadata": {},
   "source": [
    "We can query these tables:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e4bfb50-7ff2-48a2-bb88-af0a063859b8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Last SPICE files ingested into SOAR\n",
    "adql_query = \"SELECT TOP 10 filename, filesize FROM soar.v_sc_data_item WHERE instrument='SPICE' ORDER BY insertion_time DESC\"\n",
    "job = soar.launch_job(adql_query)\n",
    "# job status\n",
    "print(job)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f9a64be4-5c29-4ffe-9868-7a891d9bcf44",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = job.get_results()\n",
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "74476a5d-5588-43a2-87d3-689285179b82",
   "metadata": {},
   "source": [
    "Notes:\n",
    "\n",
    "* As we are working on `soar.v_sc_data_item`, we are missing LL data.\n",
    "* More metadata are available in instrument-specific tables (see below)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5f5f3e00-c712-41a0-a3e4-02a66974e5d0",
   "metadata": {},
   "source": [
    "## Using TOPCAT\n",
    "\n",
    "In [TOPCAT](https://www.star.bris.ac.uk/~mbt/topcat/), SOAR queries can be done by doing\n",
    "\n",
    "* File → Load Table\n",
    "* In the new window: Data sources → TAP query\n",
    "* In the new window: enter `http://soar.esac.esa.int/soar-sl-tap/tap` as `TAP URL` at the bottom of the Select Service tab, and click on the Use Service button.\n",
    "* Then in the Use Service tab, you get\n",
    "    * a tree view of all tables provided by the service, with details on each table\n",
    "    * an ADQL Text box where you have tabs containing a text box for ADQL queries\n",
    "* Once a query is run, the resulting table is loaded into the main TOPCAT window, and the data can be displayed or plotted."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9256e701-5509-4e41-9249-9038e260b64f",
   "metadata": {},
   "source": [
    "## More fun queries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "728bbdca-ddde-4003-8fd4-3d45c450eaa5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Some helper function to run a query and display the result\n",
    "def do_query(adql_query):\n",
    "    job = soar.launch_job(adql_query)\n",
    "    result = job.get_results()\n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3867948-f345-40a7-bac6-82253c7dc06a",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Get list of all file descriptors\n",
    "do_query(\"SELECT DISTINCT descriptor FROM soar.v_sc_data_item ORDER BY descriptor\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "780930d3-3e0d-4a1e-ad3b-8378fc62e608",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Get list of all SOOP names (and combinations) including \"AR-Long-Term\"\n",
    "result = do_query(\"SELECT DISTINCT soop_name FROM v_sc_data_item WHERE soop_name LIKE '%AR-Long-Term%' ORDER by soop_name\")\n",
    "result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a5cfd2a-28dc-4d94-bb19-179146c252a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Why does R_SMALL_MRES_MCAD_AR-Long-Term appear twice?\n",
    "list(filter(lambda s: ';' not in s, [row['soop_name'] for row in result]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b96837a2-d347-45c8-8a5d-3695994fa444",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Who's the culprit and when?\n",
    "do_query(\"SELECT instrument, COUNT(*) as number, MIN(begin_time) as min_date, MAX(begin_time) as max_date FROM soar.v_sc_data_item WHERE soop_name = ' R_SMALL_MRES_MCAD_AR-Long-Term' GROUP BY instrument\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f42bd477-72a9-4504-8400-4219b749aa67",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get number of L2 files and data volume per instrument for a given SOOP (this does not include combinations with other SOOPs or typos in SOOP name)\n",
    "do_query(\"SELECT instrument, COUNT(*) AS  number, SUM(filesize) AS size FROM v_sc_data_item WHERE soop_name = 'R_SMALL_MRES_MCAD_AR-Long-Term' GROUP BY instrument\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "739fd3a6-638e-488b-87ea-15ca57dd32b8",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# find SPICE L2 rasters on a specific date\n",
    "do_query(\"SELECT filename FROM soar.v_sc_data_item WHERE instrument = 'SPICE' AND level = 'L2' AND filename LIKE '%n-ras%' AND begin_time > '2022-04-02' AND begin_time < '2022-04-03' ORDER BY begin_time\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fa20b36a-41de-4d0d-9950-75305083fa00",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Now using the instrument-specific tables: soar.v_spi_sc_fits\n",
    "do_query(\"SELECT date_begin, dimension_index, binning_factor, num_pixels_per_axis, pixel_begin, pixel_end, exposure, c_delt1, c_delt2, c_delt3 FROM soar.v_spi_sc_fits WHERE filename = 'solo_L2_spice-n-ras_20220402T031537_V06_100663994-000.fits'\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1159d22-7c19-4e33-9ad7-18643b0e7838",
   "metadata": {},
   "source": [
    "For 1 file, we get 1 row per dimension, even for `CDELTn`... but there is no column for `CDELT4`. The table mixes columns that depend on the dimension index and columns that don't, this is quite strange.I guess that there should have been only one `c_delt` column, with a value depending on `dimension_index`.\n",
    "\n",
    "Also, I guess that only the headers of the first HDU are ingested into SOAR.\n",
    "\n",
    "From the tables documentation, `binning_factor` depends on the dimension index, but this is not clear from this file (we should have taken a file with binning as example)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a83cdac-d50e-485c-8bbc-bdf01a013bb8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Take data from several tables using a JOIN\n",
    "do_query(\"SELECT TOP 10 data.filename, spice.exposure, spice.data_min, spice.data_max, data.filesize FROM soar.v_sc_data_item data INNER JOIN soar.v_spi_sc_fits spice ON data.data_item_oid = spice.data_item_oid WHERE spice.dimension_index = 1 ORDER BY data.data_item_id DESC\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1734272a-b3a5-431f-bd75-fb2d4e7147d7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Last 10 EUI L2 files, with begin/end time and exposure time\n",
    "do_query(\"SELECT TOP 10 data.filename, data.begin_time, data.end_time, eui.exposure FROM soar.v_sc_data_item data INNER JOIN soar.v_eui_sc_fits eui ON data.data_item_oid = eui.data_item_oid WHERE data.instrument = 'EUI' AND data.level = 'L2' ORDER BY data.begin_time DESC\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94d6f9f5-3adc-4a90-a139-c396abc21e62",
   "metadata": {},
   "source": [
    "## Downloading a file\n",
    "\n",
    "The `data` TAP \"resource\" allows downloading data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b1f9542-afc7-4efb-861d-eb04cb37b517",
   "metadata": {},
   "outputs": [],
   "source": [
    "# With `requests`\n",
    "filename = \"solo_L2_spice-w-sit_20230405T222122_V02_184549679-000.fits\"\n",
    "adql_query = f\"SELECT filepath, filename FROM soar.v_sc_repository_file WHERE filename = '{filename}'\"\n",
    "payload = {\n",
    "    'retrieval_type': 'ALL_PRODUCTS',\n",
    "    'QUERY': adql_query\n",
    "}\n",
    "r = requests.get(f'{soar_end_point}/data', params=payload)\n",
    "print(f'HTTP GET URL for the query: {r.url}')\n",
    "r.raise_for_status()\n",
    "fits_file = \"download.fits\"\n",
    "with open(fits_file, \"wb\") as f:\n",
    "    f.write(r.content)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "95d1020d-ec79-4dfe-bcb0-d53ff2517839",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check that the file is there\n",
    "from pathlib import Path\n",
    "Path(fits_file).exists()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c8b5c21-4183-4228-8c3f-ece182095b34",
   "metadata": {},
   "source": [
    "In most cases `requests` is not needed, as building the URL and using it directly as parameter to `astropy` functions (`fits.open()`) or `sunpy` functions (`Map()`) is enough.\n",
    "\n",
    "Note: in this case, the URL content is cached by `astropy`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6854f4f5-7380-4b0a-8c24-a6b0972cec70",
   "metadata": {},
   "outputs": [],
   "source": [
    "from astropy.io import fits\n",
    "with fits.open(fits_file) as hdu_list:\n",
    "    hdu_list.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "887f886a-05a3-435d-9c03-33d63941172a",
   "metadata": {},
   "source": [
    "I don't know [how to do with `astroquery`](https://astroquery.readthedocs.io/en/latest/utils/tap.html#data-access) in the case of SOAR."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c86aa9cd-717d-478f-8977-44d726897d7e",
   "metadata": {},
   "source": [
    "## Other queries\n",
    "\n",
    "There are many other possibilities to explore, such as asynchronous queries (for larger queries), combining other tables, querying both science and LL data tables..."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
