# IAS data analysis club

Material from the IAS SPICE (and EUI) data analysis club and similar tutorials.

* 2021-04-08 Les Houches Solar Orbiter school (Éric): [Remote-sensing data hands-on](https://git.ias.u-psud.fr/teaching/ebuchlin/so-rs-tutorial/-/blob/LesHouches2021/hands-on.md)
* 2022-06-02 Sète Solar Orbiter school (Éric): [EUI/SPICE/STIX analysis](https://git.ias.u-psud.fr/teaching/ebuchlin/so-rs-tutorial/-/blob/Sete2022/hands-on-notebook.ipynb)
* 2022-12-15 IAS analysis club (Gabriel): co-alignment and jitter correction, with [SPICE stew](https://github.com/gpelouze/spice_stew/) and [SPICE jitter correction](https://github.com/gpelouze/spice_jitter_correction)
* 2023-03-21 IAS analysis club (Antoine): [co-alignement tool](https://git.ias.u-psud.fr/adolliou/alignement_tool)
* 2023-05-16 IAS analysis club (Éric): [coordinates and remapping](20230516-coordinates/coordinates.ipynb)
* 2023-06-27 IAS analysis club (Éric): [sospice demo](20230627-sospice/sospice-demo.ipynb)
* 2023-12-05 IAS analysis club (Éric): [SOAR TAP API](20231205-soar-tap/tap-demo.ipynb)

The date indicates the date of the initial tutorial, but material can be updated at a later date.

See also the [tutorials from the ESA web page](https://www.cosmos.esa.int/web/solar-orbiter/data-tutorials).
